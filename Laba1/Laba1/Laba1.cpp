﻿#include <windows.h> // підключення бібліотеки з функціями API
#include <string>
#include "resource.h"

using namespace std;
// Глобальні змінні:
HINSTANCE hInst; 	//Дескриптор програми	
LPCTSTR szWindowClass = L"Window";
LPCTSTR szTitle = L"Носик, Сироватський";
int currentIconId=IDI_ICON_GROOT;

// Попередній опис функцій

ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
// Основна програма 
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,
	int nCmdShow)
{
	MSG msg;
	// Реєстрація класу вікна 
	MyRegisterClass(hInstance);

	// Створення вікна програми
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}
	// Цикл обробки повідомлень
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS | CS_DROPSHADOW;
	wcex.lpfnWndProc = (WNDPROC)WndProc; 		//віконна процедура
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance; 			//дескриптор програми
	wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION); 		//визначення іконки
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW); 	//визначення курсору
	wcex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	
	wcex.lpszMenuName = MAKEINTRESOURCE(IDR_MENU2); 				
	wcex.lpszClassName = szWindowClass; 		//ім’я класу
	wcex.hIconSm = NULL;
	return RegisterClassEx(&wcex); 			//реєстрація класу вікна
}

// FUNCTION: InitInstance (HANDLE, int)
// Створює вікно програми і зберігає дескриптор програми в змінній hInst

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;
	hInst = hInstance; //зберігає дескриптор додатка в змінній hInst
	hWnd = CreateWindow(szWindowClass, 	// ім’я класу вікна
		szTitle, 				// назва програми
		 WS_CAPTION | WS_BORDER | WS_HSCROLL | WS_SYSMENU,			// стиль вікна
		10, 			// положення по Х	
		30,			// положення по Y	
		400, 			// розмір по Х
		300, 			// розмір по Y
		NULL, 					// дескриптор батьківського вікна	
		NULL, 					// дескриптор меню вікна
		hInstance, 				// дескриптор програми
		NULL); 				// параметри створення.

	if (!hWnd) 	//Якщо вікно не творилось, функція повертає FALSE
	{
		return FALSE;
	}
	ShowWindow(hWnd, SW_MINIMIZE); 		//Показати вікно
	UpdateWindow(hWnd); 				//Оновити вікно
	return TRUE;
}

// FUNCTION: WndProc (HWND, unsigned, WORD, LONG)
// Віконна процедура. Приймає і обробляє всі повідомлення, що приходять в додаток
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
	return FALSE;
}
LRESULT CALLBACK Close(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			PostQuitMessage(0);
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
			break;
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
			break;
		default:
			return FALSE;
			break;
		}
		break;
		
	}
	return FALSE;
}
LRESULT CALLBACK NextLevel(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND hEdit1 = GetDlgItem(hDlg, IDC_EDIT1);


	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		
		case IDCANCEL:
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
			break;
		case BTNREAD:
			TCHAR buff[1024];
			GetWindowText(hEdit1, buff, 1024);
			MessageBox(hDlg, buff, L"Введений текст", MB_OK);
			return TRUE;
			break;
		default:
			return FALSE;
			break;
		}
		break;

	}
	return FALSE;
}
NOTIFYICONDATA createTrayIcon(int ICON_ID, HWND hWnd) {
	NOTIFYICONDATA tray;
	tray.cbSize = sizeof(NOTIFYICONDATA);
	tray.hWnd = hWnd;
	tray.uID = ICON_ID;
	tray.uFlags = NIF_ICON;
	tray.uCallbackMessage = NULL;
	tray.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(ICON_ID));

	return tray;
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	RECT rt;
	HDC hdc;
	HMENU menu;
	NOTIFYICONDATA tray;
	tray.cbSize = sizeof(NOTIFYICONDATA);
	tray.hWnd = hWnd;
	tray.uID = currentIconId;
	tray.uFlags = NIF_ICON;
	tray.uCallbackMessage = NULL;
	tray.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(currentIconId));

	switch (message)
	{
	case WM_CREATE: 				//Повідомлення приходить при створенні вікна
		SetClassLong(hWnd, GCL_HICON, (LONG)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_GROOT)));
		Shell_NotifyIcon(NIM_ADD, &tray);
		break;

	case WM_PAINT: 				//Перемалювати вікно
		hdc = BeginPaint(hWnd, &ps); 	//Почати графічний вивід	
		GetClientRect(hWnd, &rt); 		//Область вікна для малювання
		DrawText(hdc, L"Носик, Сироватський КІУКІ-19-8", -1, &rt, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		EndPaint(hWnd, &ps); 		//Закінчити графічний вивід	

		break;

	case WM_CLOSE: 				//Завершення роботи
		DialogBox(hInst, (LPCTSTR)IDD_CLOSE, hWnd, (DLGPROC)Close);
		Shell_NotifyIcon(NIM_ADD, &createTrayIcon(currentIconId, hWnd));

		
		break;
	case WM_DESTROY:
		Shell_NotifyIcon(NIM_DELETE, &createTrayIcon(currentIconId, hWnd));


		break;
	case WM_LBUTTONDBLCLK: 		//Подвійне натискання лівої кнопки миші
		MessageBox(hWnd, L"Носик, Сироватський", L"КІУКІ-19-8", MB_OK | MB_ICONQUESTION);

		break;
	case WM_COMMAND:

		switch (LOWORD(wParam))
		{
	
		case MENU_IC1:
			SetClassLong(hWnd, GCL_HICON, (LONG)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_GROOT)));
			tray.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_GROOT));
			Shell_NotifyIcon(NIM_MODIFY, &tray);

			break;
		case MENU_IC2:
			SetClassLong(hWnd, GCL_HICON, (LONG)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_GOMORA)));
			tray.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_GOMORA));
			Shell_NotifyIcon(NIM_MODIFY, &tray);
			break;
		case MENU_IC3:
			SetClassLong(hWnd, GCL_HICON, (LONG)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_ROCKET)));
			tray.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON_ROCKET));
			Shell_NotifyIcon(NIM_MODIFY, &tray);
			break;
		case MENU_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUT, hWnd, (DLGPROC)About);
			break;
		case MENU_NEXTLEVEL:
			DialogBox(hInst, (LPCTSTR)IDD_NEXTLEVEL, hWnd, (DLGPROC)NextLevel);

			break;
		case MENU_CHANGE:
			menu= LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU1));
			SetMenu(hWnd, menu);

			break;
		case MENU_NORMAL:
			menu = LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU2));
			SetMenu(hWnd, menu);

			break;

		}


	default:
		//Обробка повідомлень, які не оброблені користувачем
				return DefWindowProc(hWnd, message, wParam, lParam);
	}
	

	return 0;
}

